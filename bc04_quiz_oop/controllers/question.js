export let checkList = [];
export const checkAnswer = () => {
    if (!document.querySelector(`
    input[name="singleChoice"]:checked`)) {
        return false;
    }
    let user = document.querySelector(`
    input[name="singleChoice"]:checked`).value;
    return user == "true";
}

export const number = () => {
    let a = document.querySelector(`input[name="singleChoice"]:checked`).id;
    checkList.push(a);
    console.log("Number", a);
    console.log("list", checkList);
}
export const checkFill = () => {
    let a = document.getElementById("fillInput");
    return a.dataset.notAnswer == a.value;
}
const renderRadio = (data, index) => {

    return `
 <br><div class="btn-group" data-toggle="buttons">
    <label class="btn btn-success">
      <input type="radio" name="singleChoice" value=${data.exact} id="${index}" autocomplete="off">${data.content}
    </label>
  </div>
`
}
const renderInput = (question) => {
    return `<div class="form-group">
    <label for=""></label>
    <input type="text" data-not-answer="${question.answers[0].content}"
      class="form-control" name="" id="fillInput" aria-describedby="helpId" placeholder="Nhập câu trả lời">
  </div>`
}

export const renderSingleChoice = (question) => {
    let a = "";
    let b = question.content;
    question.answers.forEach((item, index) => {
        a += renderRadio(item, index);
    });
    return b + a;
}

export const renderFillToInput = (question) => {
    let a = question.content;
    a += renderInput(question)

    return a;
}

export const renderQuestion = (question) => {
    if (question.questionType == 1) {
        document.getElementById("contentQuiz").innerHTML = renderSingleChoice(question);
    } else {
        document.getElementById("contentQuiz").innerHTML = renderFillToInput(question);
    }
}