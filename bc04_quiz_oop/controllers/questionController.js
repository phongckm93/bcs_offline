export const checkAnswerRadioButton = () => {
  if (!document.querySelector('input[name="singleChoice"]:checked')) {
    return false;
  }

  let userAnswer = document.querySelector(
    'input[name="singleChoice"]:checked'
  ).value;
  // console.log("userAnswer: ", userAnswer);

  // return userAnswer == "true";
  if (userAnswer == "true") {
    return true;
  } else {
    return false;
  }
};

export const checkFillToInput = () => {
  let inputEl = document.getElementById("fillToInput");
  // console.log(inputEl.dataset);

  return inputEl.dataset.notAnswer == inputEl.value;
};

const renderRadioButton = (data) => {
  return ` <div class="btn-group col-12 text-left" data-toggle="buttons">
    <label class="btn text-left ">
      <input type="radio" value=${data.exact} name="singleChoice" id="" autocomplete="off"> ${data.content}
    </label>
        </div>
        `;
};
// b4buttonradio
export const renderSingleChoice = (question) => {
  let contentAnswer = "";
  question.answers.forEach((item) => {
    contentAnswer += renderRadioButton(item);
  });
  // returrn question.content;
  return `<div>
  <p>${question.content}</p>
  <div class="row">
  ${contentAnswer}
  </div>

  </div>`;
};

export const renderFillToInput = (question) => {
  return `
  <div>
      <p>${question.content}</p>
      <div class="form-group">
        <input type="text"
        data-not-answer='${question.answers[0].content}'
          class="form-control" name="" id="fillToInput" aria-describedby="helpId" placeholder="Câu trả lời">
      </div>

  </div>
  `;
};

export const renderQuestion = (question) => {
  if (question.questionType == 1) {
    document.getElementById("contentQuiz").innerHTML =
      renderSingleChoice(question);
  } else {
    document.getElementById("contentQuiz").innerHTML =
      renderFillToInput(question);
  }
};

export const showResult = () => {
  document.getElementById("endQuiz").style.display = "block";
  document.getElementById("startQuiz").style.display = "none";
};
