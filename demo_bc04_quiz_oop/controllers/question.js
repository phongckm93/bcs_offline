export const renderSingleChoice = (question) => {
    return question.content;
}

export const renderFillToInput = (question) => {
    return question.content;
}

export const renderQuestion = (question) => {
    if (question.questionType == 1) {
        document.getElementById("contentQuiz").innerHTML = renderSingleChoice(question);
    } else {
        document.getElementById("contentQuiz").innerHTML = renderFillToInput(question);
    }
}

export let checkBox = (question) => {
    let array = question.answers;
    if (array.length == 1) {
        let contentAdd = `<input type="text">`
        document.getElementById("result").innerHTML = contentAdd;
        return;
    } else {
        let contentAdd = "";
        for (let i in array) {
            let result = array[i].content;
            let uotput = `
        <div class="form-check">
        <label class="form-check-label">
          <input type="radio" name="flexRadioDefault" class="form-check-input" onclick="boolean('${array[i].exact}')">
         ${result}
        </label>
      </div>
    `
            contentAdd += uotput;
        }
        document.getElementById("result").innerHTML = contentAdd;
    }

}
export var number = null;
export let boolean = (a) => {
    number = a;
    return a;
};
