var ss1 = 2 >= 2;
console.log("ss1: ", ss1);

var passwordUser = "alice_2.3";

var passwordSystem = "alice_2.3";

var ss2 = passwordUser === passwordSystem;
var ss3 = 3 !== "3";
console.log("ss3: ", ss3);

console.log("ss2: ", ss2);

//  && ||

var ss10 = 2 > 1;

var ss11 = 2 > 10;

var ss12 = ss11 || ss10 || true || false;
console.log("ss12: ", ss12);

// ss && : chỉ trả về true khi tất cả điều kiện là true

// ss || : chỉ trả về false  khi tất cả điều kiện là false

// ||

// con có tiền ,nhưng con không có nhà , nhưng mà cháu đẹp trai, cháu lười biếng, ở dơ
