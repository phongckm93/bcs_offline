//object~array pass by reference

var pull = {
    name: "pull",
    age: 2,
    gender: "Male",
    address: "Cao Thang",
    bark: function () {
        console.log("Meo Meo");
    },
}
console.log(pull.name);
pull.name = "Milo"
console.log(pull.name);
pull.bark()


var miu = pull;
console.log("pull.name", pull.name);

//dynamic key

var key = "gender";
pull[key];
console.log('pull[key]: ', pull[key]);
console.log("ok", pull["name"]);

