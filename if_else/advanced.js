// advance
// &&
function onNotificationSuccess() {
  console.log("onNotificationSuccess - Đăng nhập thành công");
}
function onNotificationFail() {
  console.log("onNotificationSuccess - Đăng nhập thất bại");
}
var isLogin = true;

isLogin && onNotificationSuccess();
!isLogin && onNotificationFail();

//  ||
var username = "Bob ";
var userLocal = username || "Alice";

if (!username) {
  userLocal = "alice ";
} else {
}
console.log("userLocal", userLocal);

// if( null){

// }
