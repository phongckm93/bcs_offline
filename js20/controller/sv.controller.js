function layThongTin() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const toanSv = document.getElementById("txtDiemToan").value * 1;
  const vanSv = document.getElementById("txtDiemVan").value * 1;
  const loaiSv = document.getElementById("loaiSV").value;

  var thongTin = {
    id: maSv,
    name: tenSV,
    type: loaiSv,
    math: toanSv,
    literature: vanSv,
    getScore: function () {
      return (this.math + this.literature) / 2;
    },
    getRank: function () {
      var dtb = this.getScore();
      if (dtb > 5) {
        return "ok";
      } else {
        return "bad";
      }
    },
  };
  return thongTin;
}

function showThongTin(k) {
  document.getElementById("spanMaSV").value = k.id;
  document.getElementById("spanTenSV").value = k.name;
  document.getElementById("spanDTB").value = k.getScore();
  document.getElementById("spanXepLoai").value = k.getRank();
  document.getElementById("spanLoaiSV").value = k.type;
}
