function SinhVien(maSv, tenSV, loaiSV, diemToan, diemVan) {
  this.id = maSv;
  this.name = tenSV;
  this.typ = loaiSV;
  this.mat = diemToan;
  this.literature = diemVan;
  this.getScore = function () {
    return (this.math + this.literature) / 2;
  };
  this.getRank = function () {
    var dtb = this.getScore();
    if (dtb > 5) {
      return "ok";
    } else {
      return "bad";
    }
  };
}
