

let dog1 = {
    name: "bull",
    age: 2,

}
// let dog2=dog1;
// let dog2 = { ...dog1 }
let dog2 = { ...dog1, name: "Alice" }

// dog2.name = "Alice";
console.log("dog1", dog1.name);
console.log('dog2 ', dog2.name);
let dogArr = [dog1, dog2];
let dog3 = {
    name: "tom",
    age: 2,
};
let dogArr2 = [...dogArr, dog3]
console.log('dogArr2: ', dogArr2);