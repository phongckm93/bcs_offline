
import { MonAnV2 } from "../../models/v2/models.v2.js";

export let layInfo = () => {
    let ma = document.getElementById("foodID").value;
    let ten = document.getElementById("tenMon").value;
    let loai = document.getElementById("loai").value;
    let gia = document.getElementById("giaMon").value;
    let khuyenMai = document.getElementById("khuyenMai").value;
    let moTa = document.getElementById("moTa").value;
    let hinh = document.getElementById("hinhMon").value;
    let tinhTrang = document.getElementById("tinhTrang").value;


    let monAn = new MonAnV2(ma, ten, loai, gia, khuyenMai, tinhTrang, hinh, moTa);
    return monAn;
}


export let renderListMonAn = (food) => {
    let contentHTML = "";
    food.forEach((item) => {
        // let b = item.ma
        let a = `
<tr>
<td>${item.ma}</td>
<td>${item.ten}</td>
<td>${item.loai == "0" ? "Chay" : "Mặn"}</td>
<td>${item.gia}</td>
<td>${item.khuyenMai}%</td>
<td>${item.tinhGiaKM()}</td>
<td>${item.tinhTrang}</td>
<td><button type="button" class="btn btn-danger" onclick="edit('${item.ma}')" data-toggle="modal" data-target="#exampleModal">Edit</button>
<button type="button" class="btn btn-danger" onclick="xoa('${item.ma}')">Delete</button></td>
</tr>
</tr>
`
        contentHTML += a;
    });

    document.getElementById("tbodyFood").innerHTML = contentHTML;
}

export let show = (c, b) => {
    let index = b.findIndex((item) => {
        return item.ma == c;
    });
    let a = b[index];

    document.getElementById("tenMon").innerText = a.ten;
    document.getElementById("loai").innerText = a.loai;
    document.getElementById("giaMon").innerText = a.gia;
    document.getElementById("khuyenMai").innerText = a.khuyenMai;
    document.getElementById("moTa").innerText = a.moTa;
    document.getElementById("hinhMon").innerText = a.hinh;
    document.getElementById("tinhTrang").innerText = a.tinhTrang;
}