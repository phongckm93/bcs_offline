import { MonAnV1 } from "../../models/v1/models.v1.js";

let layInfo = () => {
    let ma = document.getElementById("foodID").value;
    let ten = document.getElementById("tenMon").value;
    let loai = document.getElementById("loai").value;
    let gia = document.getElementById("giaMon").value;
    let khuyenMai = document.getElementById("khuyenMai").value;
    let moTa = document.getElementById("moTa").value;
    let hinh = document.getElementById("imgMonAn").value;
    let tinhTrang = document.getElementById("tinhTrang").value;


    let monAn = new MonAnV1(ma, ten, loai, gia, khuyenMai, tinhTrang, hinh, moTa);
    return monAn;
}
let showMonAn = (monAn) => {
    let { ma, ten, loai, gia, khuyenMai, tinhTrang, hinh, moTa } = monAn;
    document.getElementById("spMa").innerText = ma;
    document.getElementById("spTenMon").innerText = ten;
    document.getElementById("spLoaiMon").innerText = loai;
    document.getElementById("spGia").innerText = gia;
    document.getElementById("spKM").innerText = khuyenMai * 100;
    document.getElementById("spTT").innerText = tinhTrang;
    document.getElementById("pMoTa").innerText = moTa;
    document.getElementById("hinhMon").innerText = hinh;
    document.getElementById("spGiaKM").innerText = monAn.tinhGiaKM();

}

document.getElementById("btnThemMon").addEventListener("click", function () {
    let a = layInfo();
    showMonAn(a);
})