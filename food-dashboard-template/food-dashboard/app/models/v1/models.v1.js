
export class MonAnV1 {
    constructor(ma, ten, loai, gia, khuyenMai, tinhTrang, hinh, moTa) {
        this.ma = ma;
        this.ten = ten;
        this.loai = loai;
        this.gia = gia;
        this.khuyenMai = khuyenMai;
        this.tinhTrang = tinhTrang;
        this.hinh = hinh;
        this.moTa = moTa;
    }
    tinhGiaKM = function () {
        return this.gia * (1 - this.khuyenMai);
    }
}