
// // function weight() {
// //     var w = 100;
// //     if (w > 60) {
// //         var nickname = "fat"
// //         console.log(nickname + w + "kg");
// //     }
// //     console.log(nickname);
// //     console.log(w);
// // }
// // weight();
// /// Các method của array...
// let a = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// a.forEach(element => {
//     element++;
//     console.log('element', element)
// });
// let b = a.map((item) => item + 1)
// console.log('b', b)


// /// Dom bởi getElement
// document.getElementById("check_1").onclick = () => {

//     document.getElementById("lorem_1").innerHTML = "Hello word";
//     document.getElementsByClassName("lorem")[0].style.color = "red";
//     document.getElementsByClassName("lorem")[1].style.color = "blue";
//     document.getElementsByTagName("h1")[0].style.background = "blue";
//     document.getElementsByClassName("container")[0].getElementsByTagName("*")[2].style.background = "pink";
// }
// /// Dom bởi querySelector
// document.getElementById("check_2").addEventListener("click", () => {
//     document.querySelector(".container>h1").innerHTML = "Hello word";
//     document.querySelector(".container .lorem").style.color = "red";
//     document.querySelector(".container .lorem.display-3").style.color = "blue";
// })
// /// DOM  bởi querySelectorAll
// let check_3 = () => {
//     document.querySelectorAll("h1")[0].innerHTML = "Hello Saturday";
//     let a = document.querySelectorAll("h1");
//     a.forEach(item => {
//         item.style.color = "pink"
//     });
// }



// class Fruit {
//     constructor(title, price) {
//         this.title = title;
//         this.price = price;
//     }
//     priceInfo() {
//         return `Price of one ${this.title} is  ${this.price}$`;
//     }
// }

// let apple = new Fruit("Apple", 2);
// console.log(apple.priceInfo());

// check()

// function check() {
//     console.log('1233', 1233)
// }



// const value = { number: 10 };

// const multiply = (x = { ...value }) => {
//     console.log((x.number *= 2));
// };

// multiply();
// multiply();
// multiply(value);
// multiply(value);



// const phones = [
//     { id: 1, model: "iphone 1", price: 10000 },
//     { id: 2, model: "iphone 2", price: 11000 },
//     { id: 3, model: "iphone 3", price: 12000 },
//     { id: 4, model: "iphone 4", price: 13000 },
//     { id: 5, model: "iphone 5", price: 14000 },
// ]
// let newPhone = []
// for (let key in phones) {
//     if (phones[key].price > 11000) {
//         newPhone.push(phones[key])
//     }
// }
// console.log(newPhone)
///function trong javascript có hoisting và ko có hoisting
//ok
// check()
// function check() {
//     console.log("123");
// }
//false
// a()
// var a = function check_2() {
//     console.log("1eee");
// }
// check_3()
// let check_3 = () => {
//     console.log('first')
// }

// speak();

// function speak() {
//     console.log("Hello");
// }

// Function expression
// speak(); //TypeError: speak is not a function
// function speak() {
//     console.log("hello");
// }

/// kiểm tra spell check

// var x = "12";
// var y = x.value;
// console.log('y', y)
// console.log('x', x)
/// thử tính chất isNan
// document.getElementById("check").addEventListener("click", () => {
//     let number = document.getElementById("inputNumber").value * 1;
//     console.log(number);
//     if (isNaN(number)) {
//         alert("Not a number")
//     } else {
//         alert("Value is a number")
//     }


// })
// /// add class theme dark and light
// document.getElementById("switchButton").onclick = function () {
//     document.getElementById("myBody").classList.toggle("light");
// };
