




// document.querySelector(".title").style.color = "red";

// var titleList = document.querySelectorAll(".title");
// console.log('titleList: ', titleList);



// titleList[2].style.color = "pink"
// arry,object ~pass by refernce
// number,string,boolean~ pass by value

// var numArr = [2, 4, 8, 6];
// console.log('numArr: ', numArr);

// var menu = ["bun bo", "bun rieu", "hu tieu", "chao long"]

// menu[2] = "banh canh"
// console.log('menu: ', menu);

// // length do dai array--th ko phai ht
// var soLuongMonAn = menu.length;
// console.log('soLuongMonAn: ', soLuongMonAn);

// for (var index = 0; index < menu.length; index++) {
//     var monAn = menu[index];
//     console.log('monAn: ', monAn);
// }
// chú ý phần callback cách truyền hàm theo kiểu 2


var numArr = ["2", "4", "6"];
function get(item) {
    console.log("item", item);

}
numArr.forEach(get)
//or giống trong tài liệu
numArr.map(function (number) {
    console.log("number", number);

})
// for each không thể trả dữ liệu khi sử dụng return nên map còn Each để kiểm tra thông tin
var result = numArr.map(function (number) {
    console.log("number", number);
    return number * 10;

})
console.log("resultArr", result);


function introduce(username, callback, callmargin) {
    callback(username);
    callmargin(username);
    console.log("have a nice day");

}

function sayHello(username) {
    console.log("hello " + username);
}
function sayGoodbye(user) {
    console.log("goodbye to " + user);

}
introduce("alice", sayHello, sayGoodbye)

// mutable--immutable

var menu = ["coffe", "trà sữa"]
menu.push("trà đá");
console.log('menu: ', menu);
var lastItem = menu.pop();
console.log('lastItem: ', lastItem);
console.log('menu: ', menu);
var indexcoffe = menu.indexOf("coffe")
console.log('indexcoffe: ', indexcoffe);
if (indexcoffe !== -1) {
    console.log("tim thay");

} else {
    console.log("khong tim thay");
}


var numbers = [3, 6, 8, 9, 10]
// var result = numbers.splice(2);
// console.log('result: ', result);
// console.log('numbers: ', numbers);
var result2 = numbers.splice(2, 0);
console.log('result2: ', result2);
console.log('numbers: ', numbers);




// var menuNew = menu.splice(indexcoffe, 1)
// console.log('menuNew: ', menuNew);
// console.log("menu", menu);
// var result3 = numbers.filter(function get(item) {

//     return item < 8;
// });
// console.log('result3: ', result3);